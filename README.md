CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The module allows a buyer to choose the point of receipt of the order using
Paczkomaty service of inpost.pl.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_shipping_paczkomaty

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/commerce_shipping_paczkomaty


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

* Drupal Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

* Install the Commerce Shipping Paczkomaty module as you would normally install
  a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Commerce > Configure > Shipment > Shipment
   types > Edit your shipment type and Enable paczkomaty for this shipping
   type.
3. Navigate to Administration > Commerce > Configure > Order > Checkout flow
   > Edit 'Shipping' order checkout flow and drag "Wybierz Paczkomat"
   checkout pane below "Shipping Information". Configure it choosing a
   previously created Paczkomat shipping method.


MAINTAINERS
-----------

* Pawel Bukowski (shacky7) - https://www.drupal.org/u/shacky7

Supporting organizations:

* crazyIT - https://www.drupal.org/crazyit

Sponsors:

* Design Queen - https://designqueen.eu

